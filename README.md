Selygel
========

Pasos para la instalacion en un servidor linux

#1. Descargar codigo fuente

##1.1 Clonar via git

Clonar el repositorio en el directorio deseado:

	git clone git@bitbucket.org:pmartelletti/selygel.git directorio-raiz
	
##1.2 Descargar fuente

Descargar el archivo .zip con todo el codigo desde la pagina principal del repositorio.

#2. Instalar vendors

Una vez clonado o descargado el repositorio, es necesario instalar los vendors del proyecto. Por defecto, esto se hace 
a traves de composer.

	cd directorio-raiz
	php composer.php install
	
En caso de no tener composer instalado globalmente en el servidor, se puede descargar para el proyecto en cuestion:

	curl -sS https://getcomposer.org/installer | php

#3. Crear link simbolico

Para que el contenido del sitio sea visible correctamente, es necesario crear un link simbolico debil entre el directorio
`web` y el directorio `public_html`, que es donde se encuentra la raiz del servidor web.

	ln -s directorio-raiz/web public_html

