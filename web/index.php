<?php

require_once __DIR__.'/../vendor/autoload.php';
use Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application();
$app['debug'] = true;

require '../src/services.php';

require '../src/admin_controller.php';

require '../src/controllers.php';




/** error handling */
$app->error(function (\Exception $e, $code) use ($app) {
    switch ($code) {
        case 404:
              return $app['twig']->render('404.html');
            break;
        default:
            $message = 'We are sorry, but something went terribly wrong.';
    }
    //return $response;
});

/**
 * Before Filters
 */
$app->before( function() use ( $app ) {
    $flash = $app[ 'session' ]->get( 'flash' );
    $app[ 'session' ]->set( 'flash', null );

    if ( !empty( $flash ) )
    {
        $app[ 'twig' ]->addGlobal( 'flash', $flash );
    }
});

// run app
$app->run();