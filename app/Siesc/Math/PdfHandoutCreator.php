<?php

namespace Siesc\Math;


/**
 * Description of PdfHandoutCreator
 *
 * @author pablo
 */
class PdfHandoutCreator {
    
    private $dir;
    private $tmpFile;
    private $finalFile;
    private $fileName;
    private $db;
    private $published = false;
    static $directory = "/uploads/handouts";
    
    public function __construct($db = null) {
        $this->dir = dirname($_SERVER['SCRIPT_FILENAME']) . self::$directory;
        $this->tmpFile = sprintf(
            "%s/%s", sys_get_temp_dir(), uniqid("pandoc")
        );
        $this->db = $db;
    }
    
    public static function removeHandout($handout) {
        //var_dump(sprintf("%s/%s", dirname($_SERVER['SCRIPT_FILENAME']) . self::$directory, $handout )); exit;
        return unlink(sprintf("%s/%s", dirname($_SERVER['SCRIPT_FILENAME']) . self::$directory, $handout ));
    }


    public function publishHandout($data, $update = false) {
        $this->fileName = md5(uniqid($data['title']));
        $this->finalFile = sprintf("%s/%s.pdf", $this->dir, $this->fileName);
        $this->createHandout($data['text']);
        if(!$update) return $this->saveToDb($data);
        else return sprintf("%s.pdf",$this->fileName);
    }
    
    private function saveToDb($data) {
        $data['file'] = sprintf("%s.pdf",$this->fileName);
        unset($data['publish']);
        $this->db->insert('handout', $data);
        return $this->db->lastInsertId();
    }


    public function getFile() {
        return $this->published ? $this->finalFile : NULL;
    }


    private function createHandout($markdown) {
        if(!$this->saveTmpFile($markdown)) {
            throw new CreatorException("Unable to save content to tmp file.");
        }
        // execute commands
        $command = sprintf(
            'pandoc -f markdown+pipe_tables+table_captions+grid_tables %s -o %s',
            $this->tmpFile,
            $this->finalFile
        );
        
        exec($command, $output);
        return count($output) < 1;
    }
    
    private function saveTmpFile($text) {
        return file_put_contents($this->tmpFile, $text);
    }
    
}