<?php

use Symfony\Component\HttpFoundation\Request;

$handout_admin->match("/new", function(Request $request) use ($app){
    $form = $app['form.factory']->createBuilder('form')
        ->add('id', 'hidden')
        ->add('title')
        ->add('text', 'textarea', array(
            "required" => false
        ))
    ->getForm();
    
    if ('POST' == $request->getMethod()) {
        $form->bind($request);
        if ($form->isValid()) {
            $data = $form->getData();
            // publish the handout
            $id = $app['handout_creator']->publishHandout($data);
            return $app->redirect($app['url_generator']->generate('admin_handout_edit', array('id' => $id)));
        }
    }
    
    return $app['twig']->render('admin/handout_edit.html.twig', array(
        'form' => $form->createView(),
        'form_action' => $app['url_generator']->generate("admin_handout_new")
    ));
})
    ->bind("admin_handout_new");

$handout_admin->get("/", function() use ($app){
    $sql = "SELECT * FROM handout ";
    $handout_list = $app['db']->fetchAll($sql);

    return $app['twig']->render('admin/handout_list.html.twig', array('handout_list' => $handout_list));
})
    ->bind("admin_handout_list");


$handout_admin->match("/{id}/edit", function($id, Request $request) use ($app) {
    $sql = "SELECT * FROM handout WHERE id = ? LIMIT 1";
    $handout = $app['db']->fetchAssoc($sql, array($id));
    if (!$handout) {
        // add some flash
        $app[ 'session' ]->set( 'flash', array(
            'type'    =>'danger', //other possible values include 'warning', 'info', 
            'short'   =>'Handout not found',
            'ext'     =>'The handout with id: "' . $id . '" does not exists!.',
        ) );
        
        return $app->redirect($app['url_generator']->generate("admin_handout_list"));
    }
    
    $form = $app['form.factory']->createBuilder('form', $handout)
        ->add('id', 'hidden')
        ->add('title')
        ->add('text', 'textarea', array(
            "required" => false
        ))
    ->getForm();

    if ('POST' == $request->getMethod()) {
        $form->bind($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $id = $data['id'];
            unset($data['id']);
            // borro el archivo anterior
            \Siesc\Math\PdfHandoutCreator::removeHandout($handout['file']);
            // update file
            $filename = $app['handout_creator']->publishHandout($data, true);
            $data['file'] = $filename;
            $app['db']->update('handout', $data, array('id' => $id));
            
            // add some flash
            $app[ 'session' ]->set( 'flash', array(
                'type'    =>'success', //other possible values include 'warning', 'info', 
                'short'   =>'Handout Updated',
                'ext'     =>'The handout "' .$data['title'] . '" was successfully updated!.',
            ) );

            // redirect somewhere
            return $app->redirect($app['url_generator']->generate('admin_handout_edit', array('id' => $id)));
        }
    }

    
    return $app['twig']->render('admin/handout_edit.html.twig', array(
        'form' => $form->createView(), 'handout' => $handout, 
        'form_action' => $app['url_generator']->generate('admin_handout_edit', array("id" => $handout['id'])) 
    ));
})
    ->assert('id', '\d+')
    ->bind("admin_handout_edit");

$handout_admin->match("/{id}/handout", function($id) use ($app) {
    $sql = "SELECT * FROM handout WHERE id = ? LIMIT 1";
    $handout = $app['db']->fetchAssoc($sql, array($id));
    if (!$handout) {
        $app->abort(404, "Article with $id does not exist.");
    }
    
    if( \Siesc\Math\PdfHandoutCreator::removeHandout($handout['file'])){
        $app['db']->delete('handout', array('id' => $handout['id']));
        // add some flash
        $app[ 'session' ]->set( 'flash', array(
            'type'    =>'success', //other possible values include 'warning', 'info', 
            'short'   =>'Handout Deleted',
            'ext'     =>'The handout "' . $handout['title'] . '" was successfully deleted!.',
        ) );
    } else {
        $app[ 'session' ]->set( 'flash', array(
            'type'    =>'danger', //other possible values include 'warning', 'info', 
            'short'   =>'Handout was not Deleted',
            'ext'     =>'The handout "' . $handout['title'] . '" was not deleted because the PDF could not be deleted!.',
        ) );
    }
            
    
    
    return $app->redirect($app['url_generator']->generate('admin_handout_list'));
            
})
    ->assert('id', '\d+')
    ->bind("admin_handout_delete");
