<?php

use Symfony\Component\HttpFoundation\Request;

$news_admin->get("/", function() use ($app){
    $sql = "SELECT * FROM news ";
    $news_list = $app['db']->fetchAll($sql);
    if (!$news_list) {
        $app->abort(404, "Page does not exist.");
    }

    return $app['twig']->render('admin/news_list.html.twig', array('news_list' => $news_list));
})
    ->bind("admin_news_list");


$news_admin->match("/{id}/edit", function($id, Request $request) use ($app) {
    $sql = "SELECT * FROM news WHERE id = ? LIMIT 1";
    $news = $app['db']->fetchAssoc($sql, array($id));
    if (!$news) {
        $app->abort(404, "News with $id does not exist.");
    }
    
    $form = $app['form.factory']->createBuilder('form', $news)
        ->add('id', 'hidden')
        ->add('title')
        ->add('text', 'textarea', array(
            "attr" => array("class" => "col-lg-8")
        ))
        ->add('keyword')
        ->add('published', 'choice', array(
            'choices' => array(0 => "No", 1 => "Yes")
        ))
    ->getForm();

    if ('POST' == $request->getMethod()) {
        $form->bind($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $id = $data['id'];
            unset($data['id']);
            $app['db']->update('news', $data, array('id' => $id));
            
            // add some flash
            $app[ 'session' ]->set( 'flash', array(
                'type'    =>'success', //other possible values include 'warning', 'info', 
                'short'   =>'News Updated',
                'ext'     =>'The news "' .$data['title'] . '" was successfully updated!.',
            ) );

            // redirect somewhere
            return $app->redirect($app['url_generator']->generate('admin_news_list'));
        }
    }

    
    return $app['twig']->render('admin/news_edit.html.twig', 
            array('form' => $form->createView(), 'news' => $news, 
                'form_action' => $app['url_generator']->generate('admin_news_edit', array("id" => $news['id'])) ));
})
    ->assert('id', '\d+')
    ->bind("admin_news_edit");


$news_admin->match("/new", function( Request $request) use ($app) {
    
    $article = array(
        'id' => '',
        'title' => 'New article',
        'text' => '',
        'published' => 1,
        'keyword' => '',
    );
    
    $form = $app['form.factory']->createBuilder('form', $article)
        ->add('id', 'hidden')
        ->add('title')
        ->add('text', 'textarea', array(
            "attr" => array("class" => "col-lg-8")
        ))
        ->add('keyword')
        ->add('published', 'choice', array(
            'choices' => array(0 => "No", 1 => "Yes")
        ))
    ->getForm();
    
    if ('POST' == $request->getMethod()) {
        $form->bind($request);

        if ($form->isValid()) {
            $data = $form->getData();
            // insert in database
            $id = $app['db']->insert('news', $data);
            
            // add some flash
            $app[ 'session' ]->set( 'flash', array(
                'type'    =>'success', //other possible values include 'warning', 'info', 
                'short'   =>'Article Created',
                'ext'     =>'The article "' .$data['title'] . '" was successfully created!.',
            ) );

            // redirect somewhere
            return $app->redirect($app['url_generator']->generate('admin_news_list'));
        }
    }
    
    return $app['twig']->render('admin/news_edit.html.twig', array('form' => $form->createView(), 'news' => $article, 'form_action' => $app['url_generator']->generate('admin_news_insert')));
})
    ->bind("admin_news_insert");


$news_admin->match("/{id}/delete", function($id) use ($app) {
    $sql = "SELECT * FROM news WHERE id = ? LIMIT 1";
    $article = $app['db']->fetchAssoc($sql, array($id));
    if (!$article) {
        $app->abort(404, "Article with $id does not exist.");
    }
    
    $app['db']->delete('news', array('id' => $article['id']));
            
    // add some flash
    $app[ 'session' ]->set( 'flash', array(
        'type'    =>'success', //other possible values include 'warning', 'info', 
        'short'   =>'Article Deleted',
        'ext'     =>'The page "' .$article['title'] . '" was successfully deleted!.',
    ) );
    
    return $app->redirect($app['url_generator']->generate('admin_news_list'));
            
})
    ->assert('id', '\d+')
    ->bind("admin_news_delete");