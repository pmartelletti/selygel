<?php

use Symfony\Component\HttpFoundation\Request;


$page_admin->get("/", function() use ($app){
    $sql = "SELECT * FROM page ";
    $page_list = $app['db']->fetchAll($sql);
    if (!$page_list) {
        $app->abort(404, "Post does not exist.");
    }

    return $app['twig']->render('admin/pages_list.html.twig', array('page_list' => $page_list));
})
    ->bind("admin_pages_list");

$page_admin->match("/{slug}/edit", function($slug, Request $request) use ($app) {
    $sql = "SELECT * FROM page WHERE slug = ? LIMIT 1";
    $page = $app['db']->fetchAssoc($sql, array($slug));
    if (!$page) {
        $app->abort(404, "Page $slug does not exist.");
    }
    
    $form = $app['form.factory']->createBuilder('form', $page)
        ->add('id', 'hidden')
        ->add('name')
        ->add('slug')
        ->add('text', 'textarea', array(
            "attr" => array("class" => "col-lg-8")
        ))
        ->add('markdown', 'choice', array(
            'choices' => array(0 => "No", 1 => "Yes")
        ))
        ->add('page_order')
        ->add('enabled', 'choice', array(
            'choices' => array(0 => "No", 1 => "Yes")
        ))
    ->getForm();

    if ('POST' == $request->getMethod()) {
        $form->bind($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $id = $data['id'];
            unset($data['id']);
            $app['db']->update('page', $data, array('id' => $id));
            
            // add some flash
            $app[ 'session' ]->set( 'flash', array(
                'type'    =>'success', //other possible values include 'warning', 'info', 
                'short'   =>'Page Updated',
                'ext'     =>'The page "' .$data['name'] . '" was successfully updated!.',
            ) );

            // redirect somewhere
            return $app->redirect($app['url_generator']->generate('admin_pages_list'));
        }
    }

    
    return $app['twig']->render('admin/page_edit.html.twig', 
            array('form' => $form->createView(), 'page' => $page, 
                'form_action' => $app['url_generator']->generate('admin_page_edit', array("slug" => $page['slug'])) ));
})
    ->bind("admin_page_edit");

$page_admin->match("/new", function( Request $request) use ($app) {
    
    $page = array(
        'id' => '',
        'name' => 'New Page',
        'slug' => '',
        'text' => '',
        'markdown' => 1,
        'page_order' => '0',
        'enabled' => 0
    );
    
    $form = $app['form.factory']->createBuilder('form', $page)
        ->add('id', 'hidden')
        ->add('name')
        ->add('slug')
        ->add('text', 'textarea', array(
            "attr" => array("class" => "col-lg-8")
        ))
        ->add('markdown', 'choice', array(
            'choices' => array(0 => "No", 1 => "Yes")
        ))
        ->add('page_order')
        ->add('enabled', 'choice', array(
            'choices' => array(0 => "No", 1 => "Yes")
        ))
    ->getForm();
    
    if ('POST' == $request->getMethod()) {
        $form->bind($request);

        if ($form->isValid()) {
            $data = $form->getData();
            // insert in database
            $app['db']->insert('page', $data);
            
            // add some flash
            $app[ 'session' ]->set( 'flash', array(
                'type'    =>'success', //other possible values include 'warning', 'info', 
                'short'   =>'Page Created',
                'ext'     =>'The page "' .$data['name'] . '" was successfully created!.',
            ) );

            // redirect somewhere
            return $app->redirect($app['url_generator']->generate('admin_pages_list'));
        }
    }
    
    return $app['twig']->render('admin/page_edit.html.twig', array('form' => $form->createView(), 'page' => $page, 'form_action' => $app['url_generator']->generate('admin_page_insert')));
})
    ->bind("admin_page_insert");

$page_admin->match("/{slug}/delete", function($slug) use ($app) {
    $sql = "SELECT * FROM page WHERE slug = ? LIMIT 1";
    $page = $app['db']->fetchAssoc($sql, array($slug));
    if (!$page) {
        $app->abort(404, "Page $slug does not exist.");
    }
    
    $app['db']->delete('page', array('id' => $page['id']));
            
    // add some flash
    $app[ 'session' ]->set( 'flash', array(
        'type'    =>'success', //other possible values include 'warning', 'info', 
        'short'   =>'Page Deleted',
        'ext'     =>'The page "' .$page['name'] . '" was successfully deleted!.',
    ) );
    
    return $app->redirect($app['url_generator']->generate('admin_pages_list'));
            
})
    ->bind("admin_page_delete");