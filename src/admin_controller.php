<?php

$app->get('/admin', function () use ($app) {
    return $app['twig']->render('admin.html.twig', array('app' => $app));
})
    ->bind("admin_dashboard");

// definitions
$page_admin = $app['controllers_factory'];

require_once 'page_controller.php';

$news_admin = $app['controllers_factory'];

require_once 'news_controller.php';

$handout_admin = $app['controllers_factory'];

require_once 'assignments_controller.php';



$app->mount('/admin/pages', $page_admin);
$app->mount('/admin/news', $news_admin);
$app->mount('/admin/handouts', $handout_admin);