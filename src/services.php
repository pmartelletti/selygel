<?php
use Silex\Provider\FormServiceProvider;
use Silex\Provider\TranslationServiceProvider;

// register services
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../web/views'
));

// url generatior
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
// database doctrine dbal
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_mysql',
        'dbname' => 'sophian_selygel',
        'host' => 'localhost',
        'user' => 'root', // change in production
        'password' => '0220404' // change in production
    ),
));

// add global variable to twig, pages
$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {
    // query for all pages
    $sql = "SELECT * FROM categoria ORDER BY nombre";
    $categories = $app['db']->fetchAll($sql);
    // add global variable
    $twig->addGlobal('categories', $categories);
    return $twig;
}));

$app->register(new Silex\Provider\ValidatorServiceProvider());

$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale' => 'es',
    'translator.domains' => array()
));

$app->before(function () use ($app) {
    $app['translator']->addLoader('xlf', new Symfony\Component\Translation\Loader\XliffFileLoader());
    $app['translator']->addResource('xlf', __DIR__.'/../vendor/symfony/validator/Symfony/Component/Validator/Resources/translations/validators/validators.es.xlf', 'es', 'validators');
});

$app->register(new FormServiceProvider());

$app->register(new Silex\Provider\SessionServiceProvider());

$app->register(new Silex\Provider\SwiftmailerServiceProvider(), array(
    'host' => 'host',
    'port' => '25',
    'username' => 'username',
    'password' => 'password',
    'encryption' => null,
    'auth_mode' => null
));