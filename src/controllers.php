<?php

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;

// definitions
$app->get('/', function () use ($app) {
    return $app['twig']->render('index.html.twig', array('app' => $app, "page" => array("name" => "home")));
})
    ->bind("home");

$app->get('/sobre-selygel', function () use ($app) {
    return $app['twig']->render('sobre-selygel.html.twig', array('app' => $app, 'page' => array('name' => 'about')));
})
    ->bind("about");

$app->get('/consejos', function () use ($app) {
    return $app['twig']->render('consejos.html.twig', array('app' => $app, 'page' => array('name' => 'consejos')));
})
    ->bind("consejos");
$app->get('/productos/{category}', function ($category) use ($app) {
    $sql = "SELECT p.* FROM producto p INNER JOIN categoria c ON c.id = p.id_categoria WHERE c.nombre = ?";
    $productos = $app['db']->fetchAll($sql, array($category));

    $sql = "SELECT * FROM categoria c  WHERE c.nombre = ? LIMIT 1";
    $categoryQ = $app['db']->fetchAssoc($sql, array($category));

    $sql = "SELECT * FROM categoria c";
    $categories = $app['db']->fetchAll($sql);
    if (!$productos) {
        $app->abort(404, "Cateogory $category does not exist.");
    }

    return $app['twig']->render('productos.html.twig', array('app' => $app, 'page' => array('name' => 'productos'), 'productos' => $productos, 'categoria' => $categoryQ, 'categories' => $categories));
})
    ->value('category', 'panaderia')
    ->bind('productos');



$app->match('/contacto', function (Request $request) use ($app) {
    // create the contact form
    $form = $app['form.factory']->createBuilder('form')
        ->add('nombre', 'text', array(
            'constraints' => array(new Assert\NotBlank())
        ))
        ->add('email', 'text',  array(
            'constraints' => array(new Assert\Email() )
        ))
        ->add('consulta', 'textarea', array(
            "attr" => array("class" => "col-lg-8"),
            'constraints' => array(new Assert\NotBlank() )
        ))
        ->getForm();

    if ('POST' == $request->getMethod()) {
        $form->bind($request);

        if ($form->isValid()) {
            $data = $form->getData();

            // creo el mensaje
            $message = \Swift_Message::newInstance();

            $message = \Swift_Message::newInstance()
                ->setSubject('[SELYGEL] Formulario de Contacto')
                ->setFrom(array($data['email']))
                ->setTo(array('ventas@selygel.com.ar'))
                ->setBody($data['consulta']);

            // envio el mensaje con la nueva configuracion
            // $app['mailer']->send($message);


            // add some flash
            $app[ 'session' ]->set( 'flash', array(
                'type'    =>'success', //other possible values include 'warning', 'info',
                'short'   => $data['nombre'] . ', su email se ha enviado correctamente.',
                'ext'     =>  'Gracias por contactarse con nostros!',
            ) );

            // redirect somewhere
            return $app->redirect($app['url_generator']->generate('contacto'));
        }
    }

    return $app['twig']->render('contacto.html.twig', array('app' => $app, 'page' => array('name' => 'contacto'), 'form' => $form->createView() ));
})
    ->bind("contacto");
